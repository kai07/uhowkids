/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100148
 Source Host           : localhost:3306
 Source Schema         : uhowkids

 Target Server Type    : MariaDB
 Target Server Version : 100148
 File Encoding         : 65001

 Date: 14/09/2022 01:02:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for PhotoGroup
-- ----------------------------
DROP TABLE IF EXISTS `PhotoGroup`;
CREATE TABLE `PhotoGroup`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '圖片群組ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '照片群組名稱',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
