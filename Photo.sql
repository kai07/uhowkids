/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100148
 Source Host           : localhost:3306
 Source Schema         : uhowkids

 Target Server Type    : MariaDB
 Target Server Version : 100148
 File Encoding         : 65001

 Date: 14/09/2022 01:03:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for Photo
-- ----------------------------
DROP TABLE IF EXISTS `Photo`;
CREATE TABLE `Photo`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '圖片ID',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '照片標籤',
  `group_id` bigint(255) UNSIGNED NOT NULL COMMENT '照片群組(photo group外來鍵)',
  `user_id` bigint(255) UNSIGNED NOT NULL COMMENT '發布人(user外來鍵)',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '圖片',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `Photo_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `Photo_group_id_foreign`(`group_id`) USING BTREE,
  CONSTRAINT `Photo_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `PhotoGroup` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Photo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
