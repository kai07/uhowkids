<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class UserEntity extends Entity
{


    /**
     * 使用者ID
     *
     * @var int
     */
    protected $id;

    /**
     * 名稱
     *
     * @var string
     */
    protected $name;

    /**
     * 信箱
     *
     * @var string
     */
    protected $email;

    /**
     * 密碼
     *
     * @var string
     */
    protected $password;

    /**
     * 大頭照
     *
     * @var string
     */
    protected $avatar;

    /**
     * 建立時間
     *
     * @var string
     */
    protected $createdAt;

    /**
     * 最後更新時間
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * 刪除時間
     *
     * @var string
     */
    protected $deletedAt;

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $dates = [];
}
