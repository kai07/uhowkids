<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class ActivityParticipationEntity extends Entity
{

    /**
     * 實物捐贈資料表ID
     *
     * @var int
     */
    protected $id;

    /**
     * 活動ID(外來鍵)
     *
     * @var string
     */
    protected $activity_id;

    /**
     * 使用者ID(外來鍵)
     *
     * @var string
     */
    protected $user_id;


    /**
     * 建立時間
     *
     * @var string
     */
    protected $createdAt;

    /**
     * 最後更新時間
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * 刪除時間
     *
     * @var string
     */
    protected $deletedAt;

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $dates = [];
}
