<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class PhotoGroupEntity extends Entity
{


    /**
     * 圖片群組ID
     *
     * @var int
     */
    protected $id;

    /**
     * 群組名稱
     *
     * @var string
     */
    protected $name;

    /**
     * 建立時間
     *
     * @var string
     */
    protected $createdAt;

    /**
     * 最後更新時間
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * 刪除時間
     *
     * @var string
     */
    protected $deletedAt;

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $dates = [];
}
