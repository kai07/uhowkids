<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class ActivityExpensesEntity extends Entity
{
    /**
     * 實物捐贈資料表ID
     *
     * @var int
     */
    protected $id;

    /**
     * 項目名稱
     *
     * @var string
     */
    protected $name;

    /**
     * 項目總價格
     *
     * @var string
     */
    protected $total_price;

    /**
     * 項目平均價格
     *
     * @var string
     */
    protected $total_avg;

    /**
     * 活動ID(外來鍵)
     *
     * @var string
     */
    protected $activity_id;

    /**
     * 建立時間
     *
     * @var string
     */
    protected $createdAt;

    /**
     * 最後更新時間
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * 刪除時間
     *
     * @var string
     */
    protected $deletedAt;

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $dates = [];
}
