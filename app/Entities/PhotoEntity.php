<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;

class PhotoEntity extends Entity
{


    /**
     * 圖片ID
     *
     * @var int
     */
    protected $id;

    /**
     * 標籤
     *
     * @var string
     */
    protected $tag;

    /**
     * 圖片群組ID(外來鍵)
     *
     * @var string
     */
    protected $group_id;

    /**
     * 使用者ID(外來鍵)
     *
     * @var string
     */
    protected $user_id;

    /**
     * 圖片位置
     *
     * @var string
     */
    protected $photo;

    /**
     * 建立時間
     *
     * @var string
     */
    protected $createdAt;

    /**
     * 最後更新時間
     *
     * @var string
     */
    protected $updatedAt;

    /**
     * 刪除時間
     *
     * @var string
     */
    protected $deletedAt;

    protected $datamap = [
        'createdAt' => 'created_at',
        'updatedAt' => 'updated_at',
        'deletedAt' => 'deleted_at'
    ];

    protected $casts = [
        'id' => 'integer'
    ];

    protected $dates = [];
}
