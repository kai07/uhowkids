<?php

namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\User;
use App\Entities\UserEntity;
class LoginController extends BaseController
{
    use ResponseTrait;

    /**
     * [GET] 取得登入頁面
     *
     * @return void
     */
    public function index()
    {
        return view('login_view/login');
    }

    /**
     * [POST] 登入
     *
     * @return void
     */
    public function login()
    {
        $email       = $this->request->getPost('email');
        $password    = $this->request->getPost('password');
        $re_password = $this->request->getPost('re_password');

        $user_model  = new User();
        $userInfo    = $user_model->where('email',$email)->first();
        $hasUser     = $user_model->validUser($email, $password);

        if($hasUser == true){

            $session = \Config\Services::session();
            $session->set('name'  , $userInfo->name);
            $session->set('email' , $userInfo->email);
            $session->set('id'    , $userInfo->id);
            $session->set('avatar', $userInfo->avatar);
            $response = [
                'status' => 'success',
                'message' => 'Hi '.$userInfo->name.' 登入成功~',
            ];
        }else{

            $response = [
                'status' => 'fail',
                'message' => '登入失敗，帳號密碼錯誤',
            ];
        }

        return $this->response->setJSON($response);
    }


    /**
     * [GET] 登出
     *
     * @return void
     */
    public function logout()
    {
        $session = \Config\Services::session();
        $session->destroy();
        return redirect()->to(base_url('/'));
    }



    /**
     * [GET] 取得註冊頁面
     *
     * @return void
     */
    public function register()
    {
        return view('register_view/register');
    }

    /**
     * [POST] 註冊使用者
     *
     * @return void
     */
    public function register_create()
    {
        $name        = $this->request->getPost('name');
        $email       = $this->request->getPost('email');
        $password    = $this->request->getPost('password');
        $avatar      = $this->request->getFile('avatar');
        $user_model  = new User();
        $user_entity = new UserEntity();

        $hasUser = $user_model->where('email',$email)->first();

        if($hasUser){
            $response = [
                'status' => 'fail',
                'message' => '該Email已存在，請換另一組試試',
            ];
        }else{
            if(!$avatar->getClientName()){

                $user_entity->name     = $name;
                $user_entity->email    = $email;
                $user_entity->password = password_hash($password, PASSWORD_DEFAULT);
                $user_model->insert($user_entity);
                $insertID = $user_model->getInsertID();

                if ($insertID) {
                    $response = [
                        'status' => 'success',
                        'message' => '註冊成功',
                    ];
                } else {
                    $response = [
                        'status' => 'fail',
                        'message' => '註冊失敗',
                    ];
                }
            }else {
                $validated = $this->validate([
                    'avatar' => [
                        'uploaded[avatar]',
                        'mime_in[avatar,image/jpg,image/jpeg,image/gif,image/png]',
                    ],
                ]);

                if ($validated) {
                    $avatar->move('assets/avatar');
                    $user_entity->name     = $name;
                    $user_entity->email    = $email;
                    $user_entity->password = password_hash($password, PASSWORD_DEFAULT);
                    $user_entity->avatar   = $avatar->getClientName();
                    $user_model->insert($user_entity);
                    $insertID = $user_model->getInsertID();

                    if ($insertID) {
                        $response = [
                            'status' => 'success',
                            'message' => '註冊成功',
                        ];
                    } else {
                        $response = [
                            'status' => 'fail',
                            'message' => '註冊失敗',
                        ];
                    }
                } else {
                    $message = '註冊失敗，圖片錯誤，請上傳jpg,jpeg,png格式圖片，或是上傳檔案小於4096位元的圖檔';
                    $response = [
                        'status' => 'fail',
                        'message' => $message,
                    ];
                }
            }

        }



        return $this->response->setJSON($response);

    }
}
