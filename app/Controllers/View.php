<?php

namespace App\Controllers;

use App\Models\News;

class View extends BaseController
{
    public function index()
    {   
 
        $news_model  = new News();
        $query       = $news_model->select('News.id as n_id , News.title as title , News.photo as photo , News.created_at as created_at')
        ->orderBy("News.created_at", "DESC");
        // $query->join('User', 'User.id = News.user_id');
        $news       = $query->limit(10)->find();
        $data        = [
            'news'  => $news,
        ];
        return view('home_view/home',$data);
    }
    public function aboutUs()
    {
        return view('about_view/about_us');
    }
    
 
    public function donation()
    {
        return view('donation_view/donation');
    }
    
}
