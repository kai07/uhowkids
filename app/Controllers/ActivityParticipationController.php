<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ActivityParticipation;
use App\Entities\ActivityParticipationEntity;

class ActivityParticipationController extends BaseController
{
    public function index()
    {
        $activityParticipation_model  = new ActivityParticipation();
        $activityParticipation_entity = new ActivityParticipationEntity();
        $id = $this->request->getGet('id');
        $query  =   $activityParticipation_model->select('ActivityParticipation.id as ap_id , ActivityParticipation.activity_id , User.name. as name  , User.avater as avater , ActivityParticipation.created_at as created_at , ActivityExpenses.updated_at as updated_at')
            ->orderBy("ActivityParticipation.created_at", "DESC");
        $query->where('ActivityParticipation.activity_id', $id);
        $query->join('Activity', 'Activity.id = ActivityParticipation.activity_id');
        $query->join('User', 'User.id = ActivityParticipation.user_id');
        $query->findAll();

        return $this->response->setJSON($query);
    }
    public function create()
    {
        $session = \Config\Services::session();
        $user_id    = $session->get('id');
        $activity_id    = $this->request->getPost('a_id');
        $activityParticipation_model  = new ActivityParticipation();
        $activityParticipation_entity = new ActivityParticipationEntity();

        $activityParticipation_entity->activity_id  = $activity_id;
        $activityParticipation_entity->user_id      = $user_id;

        $activityParticipation_model->insert($activityParticipation_entity);
        $insertID = $activityParticipation_model->getInsertID();

        if ($insertID) {
            $response = [
                'status' => 'success',
                'message' => '參與成功',
            ];
        } else {
            $response = [
                'status' => 'success',
                'message' => '參與失敗',
            ];
        }

        return $this->response->setJSON($response);
    }

    public function delete($id)
    {
        $session = \Config\Services::session();
        $user_id    = $session->get('id');
        $activityParticipation_model  = new ActivityParticipation();
        $activityParticipation_entity = new ActivityParticipationEntity();
        $result = $activityParticipation_model->where('id', $id)->where('user_id', $user_id)->delete();
        if ($result) {

            $response = [
                'status' => 'success',
                'message' => '取消成功',
            ];
        } else {
            $response = [
                'status' => 'success',
                'message' => '取消失敗',
            ];
        }
        return $this->response->setJSON($response);
    }
}
