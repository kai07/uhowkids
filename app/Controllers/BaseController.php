<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 */
abstract class BaseController extends Controller
{
    /**
     * Instance of the main Request object.
     *
     * @var CLIRequest|IncomingRequest
     */
    protected $request;

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];

    /**
     * Constructor.
     */
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        // E.g.: $this->session = \Config\Services::session();
    }
    public static $registerRules = [
        'name'          => 'required',
        'email'         => 'required|valid_email',
        'password'      => 'required',
        're_password'   => 'required|matches[password]',
    ];
    public static $registerMessages = [
        "name" => [
            "required" => "使用者名稱不可為空"
        ],
        "email" => [
            "required" => "電子信箱不可為空",
            "valid_email" => "電子信箱格式錯誤"
        ],
        "password" => [
            "required" => "密碼不可為空"
        ],
        "re_password" => [
            "required" => "重複密碼不可為空",
            "matches" => "重複密碼錯誤"
        ],
    ];

}
