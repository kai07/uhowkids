<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ActivityExpenses;
use App\Entities\ActivityExpensesEntity;
use App\Models\ActivityParticipation;
use App\Entities\ActivityParticipationEntity;



class ActivityExpensesController extends BaseController
{
    public function index()
    {
        $activityExpenses_model  = new ActivityExpenses();
        $activityExpenses_entity = new ActivityExpensesEntity();

        $activityParticipation_model  = new ActivityParticipation();
        $activityParticipation_entity = new ActivityParticipationEntity();

        $id = $this->request->getGet('id');

        //count 參與人數
        $count = $activityParticipation_model->select('activity_id')->where('activity_id', $id);
        $count = $activityParticipation_model->countAllResults();


        $query  =   $activityExpenses_model->select('ActivityExpenses.id as ae_id , ActivityExpenses.name as name , ActivityExpenses.total_price as total_price, ActivityExpenses.total_avg as total_avg ,Activity.id as activity_id , ActivityExpenses.created_at as created_at , ActivityExpenses.updated_at as updated_at')
            ->orderBy("Activity.created_at", "DESC");
        $query->where('ActivityExpenses.activity_id', $id);
        $query->join('Activity', 'Activity.id = ActivityExpenses.activity_id');
        $data = $query->findAll();

        //項目總價格 / 參與人數 = 平均價格
        foreach ($data as $newData) {
            if ($count !== '0') {
                $newData->total_avg = $newData->total_price / $count;
            } else {
                $newData->total_avg = $newData->total_price;
            }
        }

        return $this->response->setJSON($data);
    }

    public function create()
    {
        $name       = $this->request->getPostGet('name');
        $totalPrice = $this->request->getPostGet('total_price');
        $totalAvg   = $this->request->getPostGet('total_avg');
        $activityId = $this->request->getPostGet('activity_id');

        $activityExpenses_model  = new ActivityExpenses();
        $activityExpenses_entity = new ActivityExpensesEntity();

        $activityExpenses_entity->name          = $name;
        $activityExpenses_entity->total_price   = $totalPrice;
        $activityExpenses_entity->total_avg     = $totalAvg;
        $activityExpenses_entity->activity_id   = $activityId;

        $activityExpenses_model->insert($activityExpenses_entity);
        $insertID = $activityExpenses_model->getInsertID();


        if ($insertID) {
            $response = [
                'status' => 'success',
                'message' => '新增成功',
            ];
        } else {
            $response = [
                'status' => 'success',
                'message' => '新增失敗',
            ];
        }

        return $this->response->setJSON($response);
    }
}
