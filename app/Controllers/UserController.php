<?php

namespace App\Controllers;

use App\Models\User;
use App\Entities\UserEntity;
use App\Models\News;
use App\Models\Photo;

class UserController extends BaseController
{
    public function index()
    {
        $session        = \Config\Services::session();
        $news_model     = new News();
        $count_my_news  = $news_model->where('user_id', $session->get('id'))->countAllResults();
        $photo_model    = new Photo();
        $count_my_photo = $photo_model->where('user_id', $session->get('id'))->countAllResults();

        $data = [
            'news_count'  => $count_my_news,
            'photo_count' => $count_my_photo
        ];
        return view('user_view/user', $data);
    }

    public function update()
    {
        $session = \Config\Services::session();
        $name    = $this->request->getPost('new_name');
        // $id    = $this->request->getPost('u_id');
        $avatar  = $this->request->getFile('my_avatar');
        $id      = $session->get('id');

        $user_model  = new User();
        $user_entity = new UserEntity();
        $userData    = $user_model->where('id', $id)->first();

        if (
            $_FILES['my_avatar']['error'] === UPLOAD_ERR_OK
        ) {
            $validated = $this->validate([
                'my_avatar' => [
                    'uploaded[my_avatar]',
                    'mime_in[my_avatar,image/jpg,image/jpeg,image/gif,image/png]',
                ],
            ]);

            if ($validated) {
                $avatar->move('assets/avatar');
                $userData->name     = $name;
                $userData->avatar   = $avatar->getClientName();
                $result = $user_model->where('id', $id)->save($userData);

                if ($result) {
                    $session->destroy();
                    $response = [
                        'status' => 'success',
                        'message' => '更新成功，請重新登入',
                    ];
                } else {
                    $response = [
                        'status' => 'fail',
                        'message' => '更新失敗',
                    ];
                }
            } else {
                $message = '資料更新失敗，圖片錯誤，請上傳jpg,jpeg,png格式圖片，或是上傳檔案小於4096位元的圖檔';
                $response = [
                    'status' => 'fail',
                    'message' => $message,
                ];
            }
        } else {
            $userData->name = $name;
            $result = $user_model->where('id', $id)->save($userData);
            if ($result) {
                $session->destroy();
                $response = [
                    'status' => 'success',
                    'message' => '更新成功，請重新登入',
                ];
            } else {
                $response = [
                    'status' => 'fail',
                    'message' => '更新失敗',
                ];
            }
        }


        return $this->response->setJSON($response);
    }
}
