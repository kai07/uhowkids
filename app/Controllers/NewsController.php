<?php

namespace App\Controllers;

use App\Models\News;
use App\Entities\NewsEntity;
use App\Models\User;

class NewsController extends BaseController
{
    /**
     * [GET] 最新消息頁面ALL
     *
     * @return void
     */
    public function index()
    {
        $news_model  = new News();
        $news_entity = new NewsEntity();
        $query       = $news_model->select('News.id as n_id , News.title as title , News.photo as photo ,User.name as name , User.avatar as avatar, News.created_at as created_at')
            ->orderBy("News.created_at", "DESC");
        $query->join('User', 'User.id = News.user_id');
        $news       = $query->paginate(5);
        $data        = [
            'news'  => $news,
            'pager' => $query->pager
        ];
        return view('news_view/news', $data);
    }

    /**
     * [GET] 發文頁面
     *
     * @return void
     */
    public function createNew()
    {
        return view('news_view/create');
    }

    /**
     * [GET] 最新消息頁面 BY id
     *
     * @return void
     */
    public function show($id)
    {
        $news_model  = new News();
        $news_entity = new NewsEntity();

        $news_model->select('News.id as n_id , News.title as title , News.content as content,News.photo as photo ,User.name as name , User.avatar as avatar, News.created_at as created_at')
            ->join('User', 'User.id = News.user_id');
        $news = $news_model->where('News.id', $id)->find();

        $data = [
            'news' => $news
        ];
        return view('news_view/single_news', $data);
    }

    /**
     * [POST] 發布消息
     *
     * @return void
     */
    public function create()
    {
        $title    = $this->request->getPost('title');
        $content  = $this->request->getPost('content');
        $news_pic = $this->request->getFile('news_pic');

        $session = \Config\Services::session();
        $id      = $session->get('id');

        $config = \HTMLPurifier_Config::createDefault();
        $config->set('URI.AllowedSchemes', array('data' => true));
        $config->set('HTML.Allowed', 'p,br,strong,em,u,figcaption,figure,span[style],a[href|target],img[alt|border|height|src|width]');
        $config->set('HTML.DefinitionID', 'enduser-customize.html tutorial');
        $config->set('HTML.DefinitionRev', 1);
        if ($def = $config->maybeGetRawHTMLDefinition()) {
            $def->addElement('figcaption', 'Block', 'Flow', 'Common');
            $def->addElement('figure', 'Block', 'Optional: (figcaption, Flow) | (Flow, figcaption) | Flow', 'Common');
        }

        $purifier = new \HTMLPurifier($config);
        $clean_html = $purifier->purify($content);

        $news_model  = new News();
        $news_entity = new NewsEntity();
        if (
            $_FILES['news_pic']['error'] === UPLOAD_ERR_OK
        ) {
            $validated = $this->validate([
                'news_pic' => [
                    'uploaded[news_pic]',
                    'mime_in[news_pic,image/jpg,image/jpeg,image/gif,image/png]',
                ],
            ]);

            if ($validated) {
                $news_pic->move('assets/img');
                $news_entity->title   = $title;
                $news_entity->content = $clean_html;
                $news_entity->user_id = $id;
                $news_entity->photo   = $news_pic->getClientName();

                $news_model->insert($news_entity);
                $insertID = $news_model->getInsertID();

                if ($insertID) {
                    $response = [
                        'status' => 'success',
                        'message' => '發佈成功',
                    ];
                } else {
                    $response = [
                        'status' => 'fail',
                        'message' => '發佈失敗',
                    ];
                }
            } else {
                $message = '資料更新失敗，圖片錯誤，請上傳jpg,jpeg,png格式圖片，或是上傳檔案小於4096位元的圖檔';
                $response = [
                    'status' => 'fail',
                    'message' => $message,
                ];
            }
        } else {
            $news_entity->title   = $title;
            $news_entity->content = $clean_html;
            $news_entity->user_id = $id;
            $news_entity->photo   = 'default.jpg';

            $news_model->insert($news_entity);
            $insertID = $news_model->getInsertID();

            if ($insertID) {
                $response = [
                    'status' => 'success',
                    'message' => '發佈成功',
                ];
            } else {
                $response = [
                    'status' => 'fail',
                    'message' => '發佈失敗',
                    // 'r' => $clean_html
                ];
            }
        }
        return $this->response->setJSON($response);
    }
}
