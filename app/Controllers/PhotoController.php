<?php

namespace App\Controllers;

use App\Entities\PhotoGroupEntity;
use App\Models\PhotoGroup;
use App\Models\Photo;
use App\Entities\PhotoEntity;

class PhotoController extends BaseController
{
    public function index()
    {
        $photo_model = new Photo();

        $query       = $photo_model->select('Photo.photo as photo , Photo.created_at as created_at,PhotoGroup.name as group_name')
                                   ->orderBy("created_at", "DESC")
                                   ->join('PhotoGroup', 'PhotoGroup.id = Photo.group_id');
        $photo       = $query->paginate(6);
        $data        = [
            'photo'  => $photo,
            'pager' => $query->pager
        ];
        return view('photo_view/photo',$data);
    }

    /**
     * [GET] 新增相片頁面
     *
     * @return void
     */
    public function createPhoto()
    {
        $photoGroup = new PhotoGroup();
        $photoGroupData = $photoGroup->get()->getResult();
        $data = [
            'photoGroupData' => $photoGroupData
        ];
        return view('photo_view/create_photo', $data);
    }




    /**
     * [POST] 新增相片
     *
     * @return void
     */
    public function create()
    {
        $group              = $this->request->getPostGet('group');
        $photo_model        = new Photo();
        $photo_group_model  = new PhotoGroup();
        $photo_group_entity = new PhotoEntity();

        if (
            $group == 'create'
        ) {

            $new_group = $this->request->getPostGet('new_group');
            $isValidGroup = $photo_group_model->where('name',$new_group)->get()->getResult();
            if($isValidGroup){
                $response = [
                    'status' => 'fail',
                    'message' => '相簿群組已存在',
                ];
                return $this->response->setJSON($response);
            }else{
                $insert_id = $photo_group_model->insert(['name' => $new_group]);
                if ($insert_id) {
                    $photo_group_entity->group_id = $insert_id;
                } else {
                    $response = [
                        'status' => 'fail',
                        'message' => '新增失敗',
                    ];
                    return $this->response->setJSON($response);
                }
            }
           
        }else{
            $photo_group_entity->group_id = $group;
        }

        if (
            $this->request->getFileMultiple('input-b3') && $this->request->getFileMultiple('input-b3')[0]->getClientName()
        ) {
            $status = false;
            foreach ($this->request->getFileMultiple('input-b3') as $file) {

                $session = \Config\Services::session();
                $file->move('assets/photo');
                
                $photo_group_entity->user_id  = $session->get('id');
                $photo_group_entity->photo    = $file->getClientName();
                $photo_insert =  $photo_model->insert($photo_group_entity);
                if ($photo_insert) {
                    $status = false;
                } else {
                    $status = true;
                }
            }

            if (!$status) {
                $response = [
                    'status' => 'success',
                    'message' => '新增成功',
                ];
                return $this->response->setJSON($response);
            } else {
                $response = [
                    'status' => 'fail',
                    'message' => '新增失敗',
                ];
                return $this->response->setJSON($response);
            }
        } else {
            $response = [
                'status' => 'fail',
                'message' => '請上傳圖片',
            ];
            return $this->response->setJSON($response);
        }

       
        





        // $session->setFlashdata("success", "Files uploaded");


    }
}
