<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Activity;
use App\Entities\ActivityEntity;

use App\Models\User;

class ActivityController extends BaseController
{

    public function index()
    {
        $activity_model  = new Activity();
        $activity_entity = new ActivityEntity();
        $query  =   $activity_model->select('Activity.id as a_id , Activity.title as title , Activity.content as content, Activity.start_date as start_date ,Activity.end_date as end_date , User.name as name , User.avatar as avatar, Activity.created_at as created_at')
            ->orderBy("Activity.created_at", "DESC");
        $query->join('User', 'User.id = Activity.user_id');
        $activity       = $query->paginate(5);
        $data        = [
            'activity'  => $activity,
            'pager' => $query->pager
        ];
        return view('activity_view/activity', $data);
    }

    public function create()
    {
        $title      = $this->request->getPost('title');
        $content    = $this->request->getPost('content');
        $startDate  = $this->request->getPost('start_date');
        $endDate    = $this->request->getPost('end_date');
        $session = \Config\Services::session();
        $user_id = $session->get('id');
        $activity_model  = new Activity();
        $activity_entity = new ActivityEntity();

        $activity_entity->title     = $title;
        $activity_entity->content   = $content;
        $activity_entity->start_date = $startDate;
        $activity_entity->end_date   = $endDate;
        $activity_entity->user_id   = $user_id;

        $activity_model->insert($activity_entity);
        $insertID = $activity_model->getInsertID();

        if ($insertID) {
            $response = [
                'status' => 'success',
                'message' => '新增成功',
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => '新增失敗',
            ];
        }

        return $this->response->setJSON($response);
    }

    public function show($id)
    {
        $activity_model  = new Activity();
        $activity_entity = new ActivityEntity();

        $activity_model->select('Activity.id as a_id , Activity.title as title , Activity.content as content, Activity.start_date as start_date ,Activity.end_date as end_date , User.name as name , User.avatar as avatar, Activity.created_at as created_at')
            ->join('User', 'User.id = Activity.user_id');
        $activity = $activity_model->where('Activity.id', $id)->find();
        $data = [
            'activity' => $activity
        ];

        return $this->response->setJSON($data);
    }

    public function update()
    {
        $id         = $this->request->getPost('a_id');
        $title      = $this->request->getPost('title');
        $content    = $this->request->getPost('content');
        $startDate  = $this->request->getPost('startDate');
        $endDate    = $this->request->getPost('endDate');

        $activity_model = new Activity();
        $activity_entity = new ActivityEntity();

        $activityData    = $activity_model->where('id', $id)->first();

        $activityData->title        = $title;
        $activityData->content     = $content;
        $activityData->start_date  = $startDate;
        $activityData->end_date     = $endDate;

        $result = $activity_model->where('id', $id)->save($activityData);
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => '更新成功',
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => '更新失敗',
            ];
        }

        return $this->response->setJSON($response);
    }
    public function delete($id)
    {
        $activity_model  = new Activity();
        $activity_entity = new ActivityEntity();
        $result = $activity_model->where('id', $id)->delete();
        if ($result) {
            $response = [
                'status' => 'success',
                'message' => '刪除成功',
            ];
        } else {
            $response = [
                'status' => 'fail',
                'message' => '刪除失敗',
            ];
        }
        return $this->response->setJSON($response);
    }
}
