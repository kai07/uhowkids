<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class PhotoGroup extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '圖片群組ID'
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'comment' => '照片群組名稱'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('PhotoGroup', true);
    }

    public function down()
    {
        $this->forge->dropTable('PhotoGroup');
    }
}
