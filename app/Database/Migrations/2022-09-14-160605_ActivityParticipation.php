<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ActivityParticipation extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '活動參與ID'
            ],
            'activity_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => true,
                'comment' => '活動id(user外來鍵)'
            ],
            'user_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => true,
                'comment' => '參與人(user外來鍵)'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);

        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('activity_id', 'Activity', 'id');
        $this->forge->addForeignKey('user_id', 'User', 'id');
        $this->forge->createTable('ActivityParticipation', true);
    }

    public function down()
    {
        $this->forge->dropForeignKey('Activity', 'id');
        $this->forge->dropForeignKey('User', 'id');
        $this->forge->dropTable('ActivityParticipation');
    }
}
