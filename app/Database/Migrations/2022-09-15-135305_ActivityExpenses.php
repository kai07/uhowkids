<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ActivityExpenses extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '活動開支ID'
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'comment' => '活動標題'
            ],
            'total_price' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'comment' => '項目總價格'
            ],
            'total_avg' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'comment' => '項目平均價格'
            ],
            'activity_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => true,
                'comment' => '活動id(user外來鍵)'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('activity_id', 'Activity', 'id');
        $this->forge->createTable('ActivityExpenses', true);
    }

    public function down()
    {
        $this->forge->dropForeignKey('Activity', 'id');
        $this->forge->dropTable('ActivityParticipation');
    }
}
