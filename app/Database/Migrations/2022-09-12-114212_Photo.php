<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Photo extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '圖片ID'
            ],
            'tag' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'comment' => '照片標籤'
            ],
            'group_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => TRUE,
                'comment' => '照片群組(photo group外來鍵)'
            ],
            'user_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => TRUE,
                'comment' => '發布人(user外來鍵)'
            ],
            'photo' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'comment' => '圖片'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);
        $this->forge->addPrimaryKey('id');

        $this->forge->addForeignKey('user_id', 'User','id');
        $this->forge->addForeignKey('group_id', 'PhotoGroup','id');
        $this->forge->createTable('Photo', true);
    }

    public function down()
    {
        // $this->forge->dropForeignKey('User', 'id');
        // $this->forge->dropForeignKey('PhotoGroup', 'id');
        $this->forge->dropTable('Photo');
    }
}
