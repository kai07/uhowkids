<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class User extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '使用者ID'
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'comment' => '使用者名稱'
            ],
            'email' => [
                'type' => 'VARCHAR',
                'unique' => true,
                'constraint' => '30',
                'comment' => '電子郵件'
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'comment' => '密碼'
            ],
            'avatar' => [
                'type' => 'VARCHAR',
                'null' => true,
                'constraint' => '255',
                'comment' => '個人照片'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('User', true);
    }

    public function down()
    {
        $this->forge->dropTable('User');
    }
}
