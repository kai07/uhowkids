<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Activity extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '活動ID'
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'comment' => '活動標題'
            ],
            'content' => [
                'type' => 'LONGTEXT',
                'null' => true,
                'comment' => '活動內容'
            ],
            'start_date' => [
                'type' => 'DATE',
                'null' => true,
                'comment' => '活動開始日期'
            ],
            'end_date' => [
                'type' => 'DATE',
                'null' => true,
                'comment' => '活動結束日期'
            ],
            'user_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => true,
                'comment' => '發布人(user外來鍵)'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('user_id', 'User', 'id');
        $this->forge->createTable('Activity', true);
    }

    public function down()
    {
        $this->forge->dropForeignKey('User', 'id');
        $this->forge->dropTable('Activity');
    }
}
