<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class News extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => 255,
                'unsigned' => true,
                'auto_increment' => true,
                'comment' => '消息ID'
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'comment' => '標題'
            ],
            'content' => [
                'type' => 'LONGTEXT',
                'null' => true,
                'comment' => '內文'
            ],
            'photo' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'comment' => '消息照片'
            ],
            'user_id' => [
                'type' => 'BIGINT',
                'constraint' => '255',
                'unsigned' => true,
                'comment' => '發布人(user外來鍵)'
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料建立日期'
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料更新日期'
            ],
            "deleted_at" => [
                'type' => 'TIMESTAMP',
                'null' => true,
                'comment' => '資料刪除日期'
            ]
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('user_id', 'User', 'id');
        $this->forge->createTable('News', true);
    }

    public function down()
    {
        $this->forge->dropForeignKey('User', 'id');
        $this->forge->dropTable('News');
    }
}
