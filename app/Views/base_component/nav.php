<?php $session = \Config\Services::session(); ?>
<header>
    <div class="bg-white shadow ">
        <div class="container ">
            <nav class="navbar navbar-expand-lg navbar-light bg-white ">
                <div class="container">
                    <a class="navbar-brand " href="#">
                        <img src="<?php echo base_url() . '/assets/img/logo-white.png' ?>" height="80px" alt="">

                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item me-2">
                                <a class="nav-link" aria-current="page" href="<?php echo base_url('/') ?>">首頁</a>
                            </li>
                            <li class="nav-item dropdown me-2">
                                <a class="nav-link dropdown-toggle" href="<?php echo base_url('about') ?>" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    關於我們
                                </a>
                                <ul class="dropdown-menu mt-1" aria-labelledby="navbarDropdownMenuLink" style="border-top: 2px solid #c3d8eb;">
                                    <li><a class="dropdown-item" href="<?php echo base_url('about') ?>">北爛成員</a></li>
                                </ul>
                            </li>
                            <li class="nav-item me-2">
                                <a class="nav-link" href="<?php echo base_url('news') ?>">最新消息</a>
                            </li>
                            <li class="nav-item me-2">
                                <a class="nav-link" href="<?php echo base_url('photo') ?>">相簿</a>
                            </li>
                            <li class="nav-item me-2">
                                <a class="nav-link" href="<?php echo base_url('donation') ?>">贊助斗內</a>
                            </li>
                            <?php if (!$session->has('email')) { ?>
                                <li class="nav-item me-2">
                                    <a class="nav-link" href="<?php echo base_url('login') ?>">登入</a>
                                </li>
                            <?php } else { ?>

                                <li class="nav-item dropdown me-2">
                                    <a class="nav-link dropdown-toggle" href="<?php echo base_url('user') ?>" id="user" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        個人資訊
                                    </a>
                                    <ul class="dropdown-menu mt-1" aria-labelledby="user" style="border-top: 2px solid #c3d8eb;">
                                        <li><a class="dropdown-item" href="<?php echo base_url('user') ?>">看看自己</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url('news/createNew') ?>">我要發公告</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url('photo/createPhoto') ?>">我要發照片</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item me-2">
                                    <a class="nav-link" href="<?php echo base_url('login/logout') ?>">登出</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>