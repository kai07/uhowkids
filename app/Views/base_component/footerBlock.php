<footer>
    <div class="info-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="row justify-content-center">
                        <div class="info-footer-main">
                            <img src="<?php echo base_url() . '/assets/img/logo.jpg' ?>" width="100%" height="150px" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                </div>
                <div class="col-md-4 col-12">
                    <div class="row justify-content-center">
                        <h5 class="text-white footer-title text-center">營業時間</h5>
                    </div>
                    <div class="row justify-content-center">
                        <div class="info-footer-main">
                            <p class="text-white text-center">
                                <span>周一至周日</span>
                            </p>
                            <p class="text-white text-center">
                                <span>AM 00:00 ~ PM 23:59</span>
                            </p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="copy-right-footer">
        <div class="container">
            <div class="row p-4">
                <p class="text-white text-center">Copyright <?php echo date("Y"); ?> © U HOW KIDS 孝子團 All rights reserved.</p>
                <p class="text-white text-center m-0">design by 蔡銘凱、彭昱倫</p>
            </div>
        </div>
    </div>
</footer>