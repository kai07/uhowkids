<!-- 網頁主要進入點(/) -->
<?= $this->extend('layout/normal_layout') ?>
<?= $this->section('customCss') ?>
<?= $this->endSection() ?>
<?= $this->section('main') ?>
<?= $this->include('user_view/main') ?>
<?= $this->endSection() ?>
<?= $this->section('customJs') ?>
<script>
    $('#fileUpload').on('change', function() {
        $('.avatar').removeClass('open');
    });
    $('.avatar').on('click', function() {
        $(this).addClass('open');
    });
    // added code to close the modal if you click outside
    $('html').click(function() {
        $('.avatar').removeClass('open');
    });

    $('.avatar').click(function(event) {
        event.stopPropagation();
    });

    function showImg(thisimg) {
        // console.log(thisimg);
        var file = thisimg.files[0];
        console.log(file);
        if (window.FileReader) {
            var fr = new FileReader();

            var img = document.getElementById('my-avatar');
            console.log(img);
            fr.onloadend = function(e) {
                img.style.backgroundImage = 'url(' + e.target.result + ')';
            };
            fr.readAsDataURL(file);
        }
    }
    $('#openChange').click(function() {
        console.log(1);
        $('#show_name').css('display', 'none');
        $('#change_name').css('display', '');
    })
    $('#closeChange').click(function() {
        $('#show_name').css('display', '');
        $('#change_name').css('display', 'none');
        console.log($('#new_name').val());
        $('#name_space').text($('#new_name').val());
    })

    $("form[id='update-user-form']").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById('update-user-form'));
        formData.append('_method', 'PUT');
        BaseLib.Put("/user", formData).then(
            (res) => {
                r = res.result;
                BaseLib.ResponseCheck(res).then(() => {
                    if (res.status == "success") {
                        window.location.reload();
                    }
                })

            },
            (err) => {
                console.log(err);
            })
    })
</script>
<?= $this->endSection() ?>