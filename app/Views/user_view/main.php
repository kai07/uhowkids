<?php $session = \Config\Services::session(); ?>
<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">看看自己</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">個人資訊</li>
                            <li class="breadcrumb-item" aria-current="page">看看自己</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <!-- dong gang start -->
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">看看自己</h3>
        </div>
        <div class="ship-main">
            <form id="update-user-form">
                <div class="container d-flex justify-content-center align-items-center">
                    <div class="avatar" id="my-avatar" style="background-image: url(<?php echo base_url() . '/assets/avatar/' . $session->get('avatar'); ?>);">
                        <input id="fileUpload" type="file" name="my_avatar" onchange="showImg(this)" />
                        <svg version="1.1" id="camera" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 25 15" enable-background="new 0 0 25 15" xml:space="preserve">
                            <path id="cameraFrame" fill="none" stroke="white" stroke-miterlimit="10" d="M23.1,14.1H1.9c-0.6,0-1-0.4-1-1V1.9c0-0.6,0.4-1,1-1h21.2
         c0.6,0,1,0.4,1,1v11.3C24.1,13.7,23.7,14.1,23.1,14.1z" />
                            <path id="circle" fill="none" stroke="#ffffff" stroke-width="1.4" stroke-miterlimit="12" d="M17.7,7.5c0-2.8-2.3-5.2-5.2-5.2S7.3,4.7,7.3,7.5s2.3,5.2,5.2,5.2
         S17.7,10.3,17.7,7.5z" />
                            <g id="plus">
                                <path fill="none" id="plusLine" class="line" stroke="#ffffff" stroke-linecap="round" stroke-miterlimit="10" d="M20.9,2.3v4.4" />
                                <path fill="none" class="line" stroke="#ffffff" stroke-linecap="round" stroke-miterlimit="10" d="M18.7,4.6h4.4" />
                            </g>
                        </svg>
                        <div id="openModal">
                            <span>更新照片</span>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center align-items-center">
                    <input type="text" class="form-control" aria-describedby="addon-wrapping" id="u_id" name="u_id" value="<?php echo $session->get('id'); ?>" hidden>
                    <h2 class="text-center mt-4" id="show_name"><span id="name_space"><?php echo $session->get('name'); ?></span><i class="fa-solid fa-pen h5 ms-2" style="cursor:pointer;" id="openChange"></i></h2>
                    <!-- <h2 class="text-center w-25"><input type="text" class="form-control"><i class="fa-sharp fa-solid fa-xmark"></i></h2> -->
                    <div class="input-group flex-nowrap w-50 mt-4" id="change_name" style="display: none;">
                        <input type="text" class="form-control" aria-describedby="addon-wrapping" id="new_name" name="new_name" value="<?php echo $session->get('name'); ?>">
                        <span class="input-group-text" id="addon-wrapping"><i class="fa-solid fa-check" style="cursor:pointer;" id="closeChange"></i></span>
                    </div>
                    <h4 class="text-center text-muted mt-2"><span><?php echo $session->get('email'); ?></span></h4>
                </div>
                <div class="row justify-content-center align-items-center mt-4 ">
                    <div class="col-6 mb-4">
                        <div class="d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn btn-success">保存變更</button>
                        </div>
                    </div>
                </div>


            </form>
            <div class="row justify-content-center align-items-center mt-4">
                <div class="col-md-4 col-12">
                    <p class="text-center display-4 fw-bold"><?php echo $news_count; ?></p>
                    <p class="text-center text-muted display-6 fw-bold">公告發文數量</p>
                </div>
                <div class="col-md-4 col-12">
                    <p class="text-center display-4 fw-bold"><?php echo $photo_count; ?></p>
                    <p class="text-center text-muted display-6 fw-bold">上傳圖片數量</p>
                </div>
                <div class="col-md-4 col-12">
                    <p class="text-center display-4 fw-bold">10</p>
                    <p class="text-center text-muted display-6 fw-bold">關注圖片</p>
                </div>
            </div>

        </div>

    </section>
</main>