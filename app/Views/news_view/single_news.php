<!-- 網頁主要進入點(/) -->
<?= $this->extend('layout/normal_layout') ?>
<?= $this->section('customCss') ?>
<link href="<?php echo base_url(); ?>/assets/css/blog/blog.css" rel="stylesheet">
<?= $this->endSection() ?>
<?= $this->section('main') ?>
<?= $this->include('news_view/single_news_main') ?>
<?= $this->endSection() ?>
<?= $this->section('customJs') ?>

<?= $this->endSection() ?>