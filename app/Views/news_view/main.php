<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">最新消息</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">最新消息</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>

    <section class="container section-padding border-bottom">
        <?php foreach ($news as $key) {
        ?>
            <div class="row mb-4">
                <div class="col-md-4 col-12">
                    <div style="padding: 10px 0px 20px;">
                        <img src="<?php echo base_url('assets/img') . '/' . $key->photo ?>" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-md-8 col-12">
                    <div class="blog-content">
                        <h4><a href="" class="blog-link-title"><?php echo $key->title ?></a></h4>
                        <span> <img src="<?php echo base_url('assets/img') . '/' . $key->avatar ?>" alt="Avatar" class="news_avatar"></span>
                        <span> <?php echo $key->name ?></span>
                        <span class="ml-4"> 發布於<?php echo $key->created_at ?></span>
                        <div class="mt-4 mb-4 blog-font">
                          
                        </div>
                        <a class="btn blog-link-btn" href=" <?php echo base_url('news') . '/' . $key->n_id ?>">查看更多</a>
                    </div>
                </div>
            </div>
            <hr>
        <?php } ?>

        <div class="d-flex justify-content-center align-items-center">
            <?= $pager->links() ?>
        </div>
    </section>
</main>