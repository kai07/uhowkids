<style>
    .new-main img {
        height: auto;
        width: 100%;
    }
</style>
<section class="masthead" style="background-image: url(' <?php echo base_url('assets/img') . '/' . $news[0]->photo ?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light"> <?php echo $news[0]->title; ?></h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">最新消息</li>
                            <li class="breadcrumb-item" aria-current="page"> <?php echo $news[0]->title; ?></li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>

    <section class="container section-padding border-bottom">
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold"><?php echo $news[0]->title; ?></h3>
        </div>
        <div class="ship-main">
            <div class="row">
                <div class="col-12"> <img src="<?php echo base_url('assets/img') . '/' .  $news[0]->avatar ?>" alt="Avatar" class="news_avatar"> <span class="ml-2"><?php echo $news[0]->name ?></span></div>
                <div class="col-12 mt-2"> <span class="ml-4 text-muted"> 發布於<?php echo $news[0]->created_at ?></span> </div>
            </div>
            <div class="new-main mt-4 mb-4">
                <?php echo $news[0]->content; ?>
            </div>

            <div class="row justify-content-center">
                <div class="col-6 text-center">
                    <a type="button" class="btn btn-outline-success" href="javascript:history.back()">回上頁</a>
                </div>

            </div>
        </div>

    </section>
</main>