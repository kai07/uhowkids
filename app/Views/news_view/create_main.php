<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">發佈公告</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">個人資訊</li>
                            <li class="breadcrumb-item" aria-current="page">發佈公告</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <!-- dong gang start -->
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">發佈公告</h3>
        </div>
        <div class="ship-main">

            <div id="container" class="container">
                <form id="post_news_form">
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control" id="title" name="title" placeholder="標題">
                        <label for="title">標題</label>
                    </div>
                    <div id="editor" name="content" class="shadow">
                    </div>

                    <div class="input-group mb-3 mt-4">

                        <div class="col-12">
                            <div class="row justify-content-center align-items-center">
                                <h4 class="text-center fw-bold">封面圖片</h4>
                            </div>
                        </div>
                        <div class="col-12 mb-2">
                            <div class="row justify-content-center align-items-center">
                                <input type="file" name="news_pic" onchange="showImg(this)" />
                            </div>
                        </div>
                        <div class="col-12 p-4" style="border:1px dashed #999">
                            <div class="row justify-content-center align-items-center">
                                <img id="before" src="http://100dayscss.com/codepen/upload.svg" style="width:25%;height:25%" class="img-fluid" />
                                <img id="showimg" src="" style="display:none;" class="img-fluid" />
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-outline-success mt-3">發佈</button>
                </form>

            </div>


        </div>

    </section>
</main>