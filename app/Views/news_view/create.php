<!-- 網頁主要進入點(/) -->
<?= $this->extend('layout/normal_layout') ?>
<?= $this->section('customCss') ?>
<link rel="stylesheet" href="<?php echo base_url('assets/css/content-styles.css') ?>" type="text/css">
<style>
    /* #container {
        width: 1000px;
        margin: 20px auto;
    } */

    .ck-editor__editable[role="textbox"] {
        /* editing area */
        min-height: 200px;
    }

    .ck-content .image {
        /* block images */
        max-width: 80%;
        margin: 20px auto;
    }
</style>
<?= $this->endSection() ?>
<?= $this->section('main') ?>
<?= $this->include('news_view/create_main') ?>
<?= $this->endSection() ?>
<?= $this->section('customJs') ?>
<!-- <script src="https://cdn.ckeditor.com/ckeditor5/35.1.0/super-build/ckeditor.js"></script> -->
<script src="<?php echo base_url(); ?>/assets/js/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/postboard.js"></script>
<script>
    function showImg(thisimg) {
        var file = thisimg.files[0];
        if (window.FileReader) {
            var fr = new FileReader();

            var showimg = document.getElementById('showimg');
            var before = document.getElementById('before');

            fr.onloadend = function(e) {
                showimg.src = e.target.result;
            };
            fr.readAsDataURL(file);
            showimg.style.display = 'block';
            before.style.display = 'none';
        }
    }
    $("form[id='post_news_form']").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById('post_news_form'));
        const editorData = editor.getData();
        formData.append('content', editorData);
        console.log(editorData);
        BaseLib.Post("/news", formData).then(
            (res) => {
                r = res.result;
                BaseLib.ResponseCheck(res).then(() => {
                    if (res.status == "success") {
                        window.location.reload();
                    }
                })

            },
            (err) => {
                console.log(err);
            })
    })
</script>
<?= $this->endSection() ?>