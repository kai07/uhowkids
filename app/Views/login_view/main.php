<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">登入</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">登入</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <!-- dong gang start -->
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">登入</h3>
        </div>
        <div class="ship-main">

        </div>
        <!-- dong gang end -->
        <form id="login-form" class="login-form">
            <div class="form-floating mb-3">
                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com">
                <label for="email">帳號</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                <label for="password">密碼</label>
            </div>
            <div class="d-grid gap-2 mt-3">
                <button type="submit" class="btn btn-lg btn-outline-coffee">登入</button>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="row justify-content-center align-items-center">
                        <a href="#" class="text-center text-info">忘記密碼?</a>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <div class="row justify-content-center align-items-center">
                        <a href="<?php echo base_url() . '/login/register' ?>" class="text-center text-info">還沒有帳號嗎? 那就來辦一個ㄅ!</a>
                    </div>
                </div>
            </div>
        </form>
    </section>
</main>