<!-- 網頁主要進入點(/) -->
<?= $this->extend('layout/normal_layout') ?>
<?= $this->section('customCss') ?>
<?= $this->endSection() ?>
<?= $this->section('main') ?>
<?= $this->include('login_view/main') ?>
<?= $this->endSection() ?>
<?= $this->section('customJs') ?>
<script>
    $("form[id='login-form']").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById('login-form'));
        BaseLib.Post("/login", formData).then(
            (res) => {
                r = res.result;
                BaseLib.ResponseCheck(res).then(() => {
                    if (res.status == "success") {
                        window.location = BaseLib.base_Url 
                    }
                })
            },
            (err) => {
                console.log(err);
            })
    })

</script>
<?= $this->endSection() ?>