<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">我要發照片</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">個人資訊</li>
                            <li class="breadcrumb-item" aria-current="page">我要發照片</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <!-- dong gang start -->
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">我要發照片</h3>
        </div>
        <div class="ship-main">
            <span class="text-danger">*上傳圖片時，需一次多張選取，否則可能會被蓋掉</span>
            <form id="upload-photo-form">
                <div class="mt-4 mb-4">
                    <label for="group" class="h4 fw-bold">為圖片分類吧~</label>
                    <select class="form-select" id="group" name="group" aria-label="Default select example">
                        <option value="none">選擇分類</option>
                        <option value="create">新增分類</option>
                        <?php foreach ($photoGroupData as $key) { ?>
                            <option value="<?php echo $key->id ?>"><?php echo $key->name ?></option>
                        <?php } ?>

                    </select>
                    <div class="input-group flex-nowrap mt-2" id="add_group" style="display: none;">
                        <span class="input-group-text bg-success" id="addon-wrapping"><i class="fa-solid fa-check text-white" style="cursor:pointer;" id="closeChange"></i></span>
                        <input type="text" name="new_group" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="addon-wrapping">
                    </div>
                </div>
                <input id="input-b3" name="input-b3[]" type="file" class="file" multiple data-show-upload="false" data-show-caption="true" data-msg-placeholder="Select {files} for upload...">

                <div class="mt-4 ">
                    <div class="d-grid gap-2">
                        <button type="submit" class="btn btn-outline-success">新增</button>
                    </div>

                </div>
            </form>

        </div>
        <!-- dong gang end -->

    </section>
</main>