<!-- 網頁主要進入點(/) -->
<?= $this->extend('layout/normal_layout') ?>
<?= $this->section('customCss') ?>
<?= $this->endSection() ?>
<?= $this->section('main') ?>
<?= $this->include('photo_view/create_photo_main') ?>
<?= $this->endSection() ?>
<?= $this->section('customJs') ?>
<script>
    $(document).ready(function() {
        // Optional setup: bootstrap library version will be auto-detected based on the 
        // loaded bootstrap JS library bootstrap.min.js. But if you wish to override the 
        // bootstrap version yourself, then you can set the following property before
        // the plugin init script (available since plugin release v5.5.0)
        $.fn.fileinputBsVersion = "3.3.7"; // if not set, this will be auto-derived
        // initialize plugin with defaults
        $("#input-b3").fileinput();

        // with plugin options
        $("#input-b3").fileinput({
            'showUpload': false,
            'previewFileType': 'any'
        });
    })

    $('#group').change(function() {
        // console.log($(this).val());
        if ($(this).val() == 'create') {
            $('#add_group').css('display', '');
        } else {
            $('#add_group').css('display', 'none');
        }
    })
    $("form[id='upload-photo-form']").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById('upload-photo-form'));
        if (checkRegister(formData)) return;
        BaseLib.Post("/photo", formData).then(
            (res) => {
                r = res.result;
                BaseLib.ResponseCheck(res).then(() => {
                    if (res.status == "success") {
                        window.location.reload();
                    }
                })

            },
            (err) => {
                console.log(err);
            })
    })

    function checkRegister(formData) {
        if (formData.get('group') == 'none') {
            Swal.fire(
                '錯誤!',
                '請選擇或建立分類!',
                'info'
            )
            return true;
        }
        // if (!formData.get('input-b3[]')) {
        //     Swal.fire(
        //         '錯誤!',
        //         '請選擇要上傳的圖片!',
        //         'info'
        //     )
        //     return true;
        // }
        return false;
    }
</script>
<?= $this->endSection() ?>