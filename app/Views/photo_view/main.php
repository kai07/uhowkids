<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">相簿</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">相簿</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <!-- dong gang start -->
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">相簿</h3>
        </div>
        <div class="ship-main">
            <ul class="image-gallery p-0">
                <?php foreach ($photo as $key) { ?>
                    <li>
                        <a href="<?php echo base_url('assets/img') . '/' . $key->photo; ?>" data-toggle="lightbox" data-gallery="example-gallery">
                            <img src="<?php echo base_url('assets/img') . '/' . $key->photo; ?>" alt="" class="img-fluid" />
                            <div class="overlay"><span><?php echo $key->group_name; ?></span></div>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <hr>
        <div class="d-flex justify-content-center align-items-center">
            <?= $pager->links() ?>
        </div>
        <!-- dong gang end -->

    </section>
</main>