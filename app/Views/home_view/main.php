<!-- carousel start  -->
<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="<?php echo base_url() . '/assets/img/S__5136438.jpg' ?>" class=" d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo base_url() . '/assets/img/S__5136439.jpg' ?>" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo base_url() . '/assets/img/S__5136440.jpg' ?>" class="d-block w-100" alt="...">

        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
<!-- carousel end  -->

<!-- main start-->
<main>
    <!-- ship info start -->
    <section class="container section-padding border-bottom">
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">什麼是 U HOW KIDS ?</h3>
        </div>
        <div class="ship-main">
            <p class="lead">
            <figure>
                <blockquote class="blockquote">
                    <p>孝子團，是一個以彭昱倫大帥哥為主的帥哥團體，後期也衍生了美仙女的行列，雖然成員各個千奇百怪，但始終心連一體，發生了會如何，不發生又會如何。動機，可以說是最單純的力量。我們不得不面對一個非常尷尬的事實，那就是，總而言之，孝子團似乎是一種巧合，但如果我們從一個更大的角度看待問題，這似乎是一種不可避免的事實。若能夠洞悉孝子團各種層面的含義，勢必能讓思維再提高一個層級。</p>
                </blockquote>
                <figcaption class="blockquote-footer">
                    節自 <cite title="Source Title">唬爛產生器</cite>
                </figcaption>
            </figure>

            </p>
        </div>
    </section>
    <!-- ship info end -->
    <section class="container section-padding border-bottom">
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">最新消息</h3>
        </div>
        <div class="ship-main">
            <p class="lead text-break text-center">
                活動公告、團購...等雜七雜八。
            </p>
            <ul class="image-gallery p-0">
                <?php foreach ($news as $key) { ?>
                    <li>
                        <a href="<?php echo base_url('news') . '/' . $key->n_id; ?>">
                            <img src="<?php echo base_url('assets/img') . '/' . $key->photo; ?>" alt="" />
                            <div class="overlay"><span><?php echo $key->title; ?></span></div>
                        </a>

                    </li>
                <?php } ?>
            </ul>
            <div class="d-flex justify-content-center align-items-center">
                <a type="button" class="btn btn-outline-dark" href="<?php echo base_url('news') ?>"><span class="h5">看更多</span></a>
            </div>
        </div>
    </section>

</main>

<!-- main end -->