<!-- 網頁主要進入點(/) -->
<?= $this->extend('layout/normal_layout') ?>
<?= $this->section('customCss') ?>
<?= $this->endSection() ?>
<?= $this->section('main') ?>
<?= $this->include('register_view/main') ?>
<?= $this->endSection() ?>
<?= $this->section('customJs') ?>
<script>
    function showImg(thisimg) {
        var file = thisimg.files[0];
        if (window.FileReader) {
            var fr = new FileReader();

            var showimg = document.getElementById('showimg');
            var before = document.getElementById('before');

            fr.onloadend = function(e) {
                showimg.src = e.target.result;
            };
            fr.readAsDataURL(file);
            showimg.style.display = 'block';
            before.style.display = 'none';
        }
    }
    $("form[id='register-form']").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(document.getElementById('register-form'));
        // console.log(formData.get('user_gender'));
        if (checkRegister(formData)) return;
        BaseLib.Post("/login/register_create", formData).then(
            (res) => {
                r = res.result;
                BaseLib.ResponseCheck(res).then(() => {
                if (res.status == "success") {
                    window.location = BaseLib.base_Url + "/login"
                }
                })

            },
            (err) => {
                console.log(err);
            })
    })

    function checkRegister(formData) {
        if (formData.get('password') !== formData.get('re_password')) {
            Swal.fire(
                '輸入錯誤!',
                '二次輸入的密碼不符合!',
                'info'
            )
            return true;
        }
        if (formData.get('re_password').length <= 6) {
            Swal.fire(
                '提醒!',
                '密碼必須大於6個英文字!',
                'info'
            )
            return true;
        }
        return false;
    }
</script>
<?= $this->endSection() ?>