<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">註冊</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">註冊</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <!-- dong gang start -->
        <div class="ship-title">
            <h3 class="text-center section-title fw-bold">註冊</h3>
        </div>
        <div class="ship-main">

        </div>
        <!-- dong gang end -->
        <form id="register-form" class="login-form">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
                <label for="name">暱稱</label>
            </div>
            <div class="form-floating mb-3">
                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
                <label for="email">帳號</label>
                <span class="text-danger">*盡量使用email註冊，不然到時忘記密碼重新改密的話，可能會發信失敗。</span>
            </div>
            <div class="form-floating mb-3">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                <label for="password">密碼</label>
            </div>
            <div class="form-floating mb-3">
                <input type="password" class="form-control" id="re_password" name="re_password" placeholder="Password" required>
                <label for="re_password">再次輸入密碼</label>
            </div>
            <div class="input-group mb-3">
                <div class="col-12 mb-2">
                    <div class="row justify-content-center align-items-center">
                        <input type="file" name="avatar" onchange="showImg(this)" />
                    </div>
                </div>
                <div class="col-12 p-4" style="border:1px dashed #999">
                    <div class="row justify-content-center align-items-center">
                        <img id="before" src="http://100dayscss.com/codepen/upload.svg" style="width:25%;height:25%" class="img-fluid" />
                        <img id="showimg" src="" style="display:none;" class="img-fluid" />
                    </div>
                </div>
            </div>
            <div class="d-grid gap-2 mt-3">
                <button type="submit" class="btn btn-lg btn-outline-coffee">註冊</button>
            </div>
        </form>
    </section>
</main>