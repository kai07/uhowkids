<section class="masthead" style="background-image: url('https://pixelprowess.com/i/pow_angle.webp');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-8 text-center h-50 text-white" style="background-color:rgba(0, 0, 0, 0.8)">
                <div class="w-100 h-100 d-flex justify-content-center align-items-center">
                    <div class="col-12">
                        <h1 class="fw-light">贊助斗內</h1>
                        <p class="lead ">
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item "><a class="text-white" href="<?php echo base_url('/') ?>">首頁</a></li>
                            <li class="breadcrumb-item" aria-current="page">贊助斗內</li>
                        </ol>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<main>
    <section class="container section-padding border-bottom">
        <div class="row">
            <div class="ship-title">
                <h3 class="text-center section-title fw-bold">欸! 拉贊助了欸</h3>
            </div>
            <div class="ship-main">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-4 col-12">
                        <div class="d-flex justify-content-center align-items-center">
                            <figure class="figure">
                                <img src="<?php echo base_url() . '/assets/img/Chunghwa_Post_Logo.svg.png' ?>" width="250" height="250" class="figure-img img-fluid rounded" alt="...">
                                <figcaption class="figure-caption text-center">郵局轉帳</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="ship-sub-title">
                            <h4 class="text-center section-sub-title fw-bold">郵局轉帳</h4>
                        </div>
                        <div class="ship-sub-main">
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <p class="text-center h5">帳號</p>
                                    <div class="row  p-4">
                                        <ul>
                                            <li class="text-center fw-bold">0071673 0208302</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <p class="text-center h5">分行</p>
                                    <div class="row  p-4">
                                        <ul>
                                            <li class="text-center fw-bold">琉球分行</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <p class="text-center h5">姓名</p>
                                    <div class="row  p-4">
                                        <ul>
                                            <li class="text-center fw-bold">蔡銘凱</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-4 col-12">
                        <div class="d-flex justify-content-center align-items-center">
                            <figure class="figure">
                                <img src="<?php echo base_url() . '/assets/img/Edenred_ClientPortal_ClientLogo_1903_05.png' ?>" width="250" height="250" class="figure-img img-fluid rounded" alt="...">
                                <figcaption class="figure-caption text-center">中信轉帳</figcaption>
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="ship-sub-title">
                            <h4 class="text-center section-sub-title fw-bold">中信轉帳</h4>
                        </div>
                        <div class="ship-sub-main">
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <p class="text-center h5">帳號</p>
                                    <div class="row  p-4">
                                        <ul>
                                            <li class="text-center fw-bold">(822)565540503379</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <p class="text-center h5">分行</p>
                                    <div class="row  p-4">
                                        <ul>
                                            <li class="text-center fw-bold">中國信託-右昌簡易分行</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <p class="text-center h5">姓名</p>
                                    <div class="row  p-4">
                                        <ul>
                                            <li class="text-center fw-bold">蔡銘凱</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row justify-content-center align-items-center">
                    
                    <div class="col-md-12 col-12 mt-4">
                        <div class="ship-sub-title">
                            <h4 class="text-center section-sub-title fw-bold">LINE PAY 也可以啦</h4>
                        </div>
                        <div class="ship-sub-main">
                            <div class="row">
                                <div class="col-md-12 col-12">
                                    <p class="text-center h5">直接pay給我就好了</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>