<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}
/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
$routes->get('/', 'View::index');
$routes->group(
    'about',
    [
        'namespace' => 'App\Controllers',
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'View::aboutUs');
    }
);
$routes->group(
    'news',
    [
        'namespace' => 'App\Controllers',
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'NewsController::index');
        $routes->get('(:num)', 'NewsController::show/$1');
        $routes->get('createNew', 'NewsController::createNew', ['filter' => 'auth']);
        $routes->post('/', 'NewsController::create', ['filter' => 'auth']);
    }
);

$routes->group(
    'photo',
    [
        'namespace' => 'App\Controllers',
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'PhotoController::index');
        $routes->post('/', 'PhotoController::create');
        $routes->get('createPhoto', 'PhotoController::createPhoto');
    }
);
$routes->group(
    'donation',
    [
        'namespace' => 'App\Controllers',
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'View::donation');
    }
);

$routes->group(
    'login',
    [
        'namespace' => 'App\Controllers',
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'LoginController::index');
        $routes->post('/', 'LoginController::login');
        $routes->get('logout', 'LoginController::logout');

        $routes->get('register', 'LoginController::register');
        $routes->post('register_create', 'LoginController::register_create');
    }
);
$routes->group(
    'user',
    [
        'namespace' => 'App\Controllers',
        'filter'    => 'auth'
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'UserController::index');
        $routes->put('/', 'UserController::update');
    }
);

$routes->group(
    'activity',
    [
        'namespace' => 'App\Controllers',
        // 'filter'    => 'auth'
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'ActivityController::index');
        $routes->post('/', 'ActivityController::create');
        $routes->get('(:num)', 'ActivityController::show/$1');
        $routes->put('/', 'ActivityController::update');
        $routes->delete('(:num)', 'ActivityController::delete/$1');
    }
);

$routes->group(
    'activityExpenses',
    [
        'namespace' => 'App\Controllers',
        // 'filter'    => 'auth'
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'ActivityExpensesController::index');
        $routes->post('/', 'ActivityExpensesController::create');
        $routes->get('/(:num)', 'ActivityExpensesController::show/$1');
        // $routes->get('(:num)', 'ActivityController::show/$1');
        // $routes->put('/', 'ActivityController::update');
        // $routes->delete('(:num)', 'ActivityController::delete/$1');
    }
);


$routes->group(
    'activityParticipation',
    [
        'namespace' => 'App\Controllers',
        // 'filter'    => 'auth'
    ],
    function (\CodeIgniter\Router\RouteCollection $routes) {
        $routes->get('/', 'ActivityParticipationController::index');
        $routes->post('/', 'ActivityParticipationController::create');
        $routes->get('/(:num)', 'ActivityParticipationController::show/$1');
        // $routes->get('(:num)', 'ActivityController::show/$1');
        // $routes->put('/', 'ActivityController::update');
        // $routes->delete('(:num)', 'ActivityController::delete/$1');
    }
);

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
