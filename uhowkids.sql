/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100148
 Source Host           : localhost:3306
 Source Schema         : uhowkids

 Target Server Type    : MariaDB
 Target Server Version : 100148
 File Encoding         : 65001

 Date: 14/09/2022 00:56:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for News
-- ----------------------------
DROP TABLE IF EXISTS `News`;
CREATE TABLE `News`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息ID',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '標題',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '內文',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息照片',
  `user_id` bigint(255) UNSIGNED NOT NULL COMMENT '發布人(user外來鍵)',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `News_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `News_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for Photo
-- ----------------------------
DROP TABLE IF EXISTS `Photo`;
CREATE TABLE `Photo`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '圖片ID',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '照片標籤',
  `group_id` bigint(255) UNSIGNED NOT NULL COMMENT '照片群組(photo group外來鍵)',
  `user_id` bigint(255) UNSIGNED NOT NULL COMMENT '發布人(user外來鍵)',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '圖片',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `Photo_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `Photo_group_id_foreign`(`group_id`) USING BTREE,
  CONSTRAINT `Photo_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `PhotoGroup` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `Photo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for PhotoGroup
-- ----------------------------
DROP TABLE IF EXISTS `PhotoGroup`;
CREATE TABLE `PhotoGroup`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '圖片群組ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '照片群組名稱',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for User
-- ----------------------------
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '使用者ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '使用者名稱',
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '電子郵件',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密碼',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '個人照片',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `namespace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2022-09-12-114159', 'App\\Database\\Migrations\\User', 'default', 'App', 1663084231, 1);
INSERT INTO `migrations` VALUES (2, '2022-09-12-114201', 'App\\Database\\Migrations\\PhotoGroup', 'default', 'App', 1663084231, 1);
INSERT INTO `migrations` VALUES (3, '2022-09-12-114212', 'App\\Database\\Migrations\\Photo', 'default', 'App', 1663084231, 1);
INSERT INTO `migrations` VALUES (4, '2022-09-12-114213', 'App\\Database\\Migrations\\News', 'default', 'App', 1663084231, 1);

SET FOREIGN_KEY_CHECKS = 1;
