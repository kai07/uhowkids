/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MariaDB
 Source Server Version : 100148
 Source Host           : localhost:3306
 Source Schema         : uhowkids

 Target Server Type    : MariaDB
 Target Server Version : 100148
 File Encoding         : 65001

 Date: 14/09/2022 01:03:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for News
-- ----------------------------
DROP TABLE IF EXISTS `News`;
CREATE TABLE `News`  (
  `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息ID',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '標題',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '內文',
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息照片',
  `user_id` bigint(255) UNSIGNED NOT NULL COMMENT '發布人(user外來鍵)',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料建立日期',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料更新日期',
  `deleted_at` timestamp(0) NULL DEFAULT NULL COMMENT '資料刪除日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `News_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `News_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
